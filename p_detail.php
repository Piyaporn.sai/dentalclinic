<?php
	require("mysql/config.php");
	$pid=$_GET['pid'];
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dental Clinic</title>
<style type="text/css">
a:link {
	color: #000;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #000;
}
a:hover {
	text-decoration: none;
	color: #F00;
}
a:active {
	text-decoration: none;
	color: #000;
}
body {
	background-color: #F1FBFB;
}
</style>
</head>

<body>
<table width="900" height="558" border="2" align="center" cellpadding="3">
  <tr>
    <td colspan="2" align="center" valign="top"><img src="photos/head.png" width="895" height="400" /></td>
  </tr>
  <tr>
    <td width="138" height="56" align="left" valign="top"><table width="138" border="0" align="left">
      <tr>
        <td width="132" height="33" align="right" valign="top" bgcolor="#CCCCCC"><strong>บุคคล</strong> :</td>
        </tr>
      <tr>
        <td height="29" align="right" valign="top"><a href="p_form.php?pid=<?php echo ($pid); ?>">แก้ไขข้อมูลส่วนตัว</a></td>
        </tr>
      <tr>
        <td height="30" align="right" valign="top"><a href="p_form_appoin.php?pid=<?php echo ($pid);?>">นัดการรักษา</a></td>
        </tr>
      <tr>
        <td align="right" valign="top"><a href="p_appoin_detail.php?pid=<?php echo ($pid); ?>">ผลการนัด</a></td>
        </tr>
    </table></td>
    <td width="749" align="left" valign="top"><p>&nbsp;</p>
<?php
	$pid=$_GET['pid'];
	require("p_select.php");
?>
<table align="center" cellpadding="10">
	  <caption>
	    Patient Data
  </caption>
	  <tr>
	    <td colspan="2" align="center" valign="middle"></a></td>
  </tr>
	  <tr>
	    <td width="79" align="right" valign="top">ID :</td>
	    <td width="50" align="left" valign="top"><?php echo $pid ?></td>
  </tr>
	  <tr>
	    <td align="right" valign="top">Name :</td>
	    <td align="left" valign="top"><?php echo $pname ?></td>
  </tr>
	  <tr>
	    <td align="right" valign="top">Birthday :</td>
	    <td align="left" valign="top"><?php echo $pbirthday?></td>
  </tr>
	  <tr>
	    <td align="right" valign="top">Phone :</td>
	    <td align="left" valign="top"><?php echo $pphone?></td>
  </tr>
	  <tr>
	    <td align="right" valign="top">Address :</td>
	    <td align="left" valign="top"><?php echo $paddress?></td>
  </tr>
	  <tr>
	    <td align="right" valign="top">E-mail :</td>
	    <td align="left" valign="top"><?php echo $pemail?></td>
  </tr>
	  <tr>
	    <td colspan="2" align="center" valign="bottom">
        <a href="p_form.php?pid=<?php echo($pid);?>">Edit</a> 
        &nbsp;&nbsp;
        <a href="javascript:removedata();">Remove</a>
        </td>
  </tr>
	  <tr>
	    <td colspan="2" align="center" valign="bottom">
        <a href="home_p.php?pid=<?php echo ($pid)?>">Home</a></td>
  </tr>
</table>
<script language="javascript">
	function removedata(){
	if(confirm("ยืนยันการลบข้อมูล")==true){
		window.location.href="p_delete.php?pid=<?php echo($pid);?>";
		}
	}
</script>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top"><img src="photos/footer.png" width="895" height="80" /></td>
  </tr>
</table>
</body>
</html>