<?php
	require("mysql/config.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dental Clinic</title>
<script src="SpryAssets/SpryValidationPassword.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationPassword.css" rel="stylesheet" type="text/css">
<style type="text/css">
a:link {
	color: #000;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #000;
}
a:hover {
	text-decoration: none;
	color: #F00;
}
a:active {
	text-decoration: none;
	color: #000;
}
body {
	background-color: #F1FBFB;
}
</style>
</head>

<body>
<table width="900" height="558" border="2" align="center" cellpadding="3">
  <tr>
    <td colspan="2" align="center" valign="top"><img src="photos/head.png" width="895" height="400" /></td>
  </tr>
  <tr>
    <td width="138" height="56" align="left" valign="top"><table width="138" border="0" align="left">
      <tr>
        <td width="132" height="33" align="right" valign="top" bgcolor="#CCCCCC"><strong>ทันตแพทย์</strong> :</td>
        </tr>
      <tr>
        <td height="29" align="right" valign="top"><a href="d_form.php?did=<?php echo ($did); ?>">แก้ไขข้อมูลส่วนตัว</a></td>
        </tr>
      <tr>
        <td height="30" align="right" valign="top"><a href="d_cf_appoin.php?did=<?php echo ($did); ?>">ยืนยันวันนัดการรักษา</a></td>
        </tr>
      <tr>
        <td align="right" valign="top"><a href="ass_appoin_detail.php">วันนัดการรักษา</a></td>
        </tr>
    </table></td>
    <td width="749" align="left" valign="top"><p>
    </p>
    <?php
	if(isset($_GET['did'])){
		$did=$_GET['did'];
		require("d_select.php");
		//$uaid=$_GET['aid'];
		$action=("d_update.php");
	}
	else{
		$did="";
		$dname="";
		$dbirthday="";
		$dphone="";
		$daddress="";
		$demail="";
		$dpass="";
		$action="d_insert.php";
		
	}
?>
<form action="<?php echo($action);?>" method="post" enctype="multipart/form-data" name="ass_Form" target="_self" onSubmit="return checkForm();">
  <table align="center" cellpadding="3">
    <caption>
      Dentist Form
    </caption>
    <tr>
      <td colspan="2" align="center" valign="top"><br>
        <input name="udid" type="hidden" id="udid" value="<?php echo $did ?>"></td>
    </tr>
    <tr>
      <td width="72" height="46" align="right" valign="top">ID :</td>
      <td width="186" align="left" valign="top"><input name="did" type="text" id="did" value="<?php echo($did);?>" size="5" maxlength="5"></td>
    </tr>
    <tr>
      <td height="44" align="right" valign="top">Name :</td>
      <td align="left" valign="top"><input name="name" type="text" id="name" value="<?php echo($dname);?>"></td>
    </tr>
    <tr>
      <td height="44" align="right" valign="top">Birthday :</td>
      <td align="left" valign="top"><input name="birthday" type="text" id="birthday" value="<?php echo($dbirthday);?>"></td>
    </tr>
    <tr>
      <td height="47" align="right" valign="top">Phone :</td>
      <td align="left" valign="top"><input name="phone" type="text" id="phone" value="<?php echo($dphone);?>"></td>
    </tr>
    <tr>
      <td height="50" align="right" valign="top">Address :</td>
      <td align="left" valign="top"><input name="address" type="text" id="address" value="<?php echo($daddress);?>"></td>
    </tr>
    <tr>
      <td height="47" align="right" valign="top">E-mail :</td>
      <td align="left" valign="top"><input name="email" type="text" id="email" value="<?php echo($demail);?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top">Password :</td>
      <td align="left" valign="top"><span id="sprypassword1">
        <input name="password" type="password" id="password" value="<?php echo($dpass);?>" size="8" maxlength="8">
      <span class="passwordRequiredMsg">A value is required.</span></span></td>
    </tr>
    <tr>
      <td colspan="2" align="center" valign="top"><input type="reset" name="Reset" id="button" value="Reset">
      &nbsp;&nbsp;
<input type="submit" name="button2" id="button2" value="Submit"></td>
    </tr>
    <tr>
      <td colspan="2" align="center" valign="top"><a href="javascript:window.history.back();">Back </a></td>
    </tr>
    <tr>
      <td colspan="2" align="center" valign="top"><a href="home_d.php?did=<?php echo ($did); ?>">Home</a></td>
    </tr>
  </table>
</form>
<script type="text/javascript">
var sprypassword1 = new Spry.Widget.ValidationPassword("sprypassword1");
</script>
<script language="javascript">
	function checkForm(){
	var v1 = document.getElementById('did').value;
	if(v1.length<1 || v1.length>8){
		alert("กรอก ID : ");
		document.getElementById('did').focus();
		return false;
	}
	else{
		return true;
	}
}
</script>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top"><img src="photos/footer.png" width="895" height="80" /></td>
  </tr>
</table>
</body>
</html>